---
documentclass: extarticle
fontsize: 17pt
header-includes: |
	\usepackage[margin=2cm]{geometry}
---

#Recette de gauffres
- Farine: **3 tasses (450 g)**
- Sucre: **4 cu. à table**
- Poudre à pâte: **4 cu. à thé**
- Lait: **3 tasses**
- Huile: **2 cu. à table**
- Sel: **1 pincée**

Facultatif: 2 cu. à thé de vanille, 1 cu. à thé d'extrait d'amande, ou 1/2 cu. à thé d'épice (i.e. canelle).

Fait de bons pancakes, ou bien de bonnes crèpes avec un peu plus d'eau.