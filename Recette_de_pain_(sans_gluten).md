#Recette de pain (sans gluten)
Commencez par sortir 3 oeufs du réfrigérateur.
- Sec:
	- Farine de riz blanc 320 g
	- Lait en poudre 85 g
	- Fécule de patate 80 g - 95 g
	- Fécule de tapioca 70 g
	- Fécule de maïs 45 g
	- Sel 11 g
	- Gomme xanthane 9 g - 10 g
- Liquides:
	1) Eau tiède $1\frac{1}{2}$ tasse
	2) Vinaigre de cidre 1 cu. à table
	3) Sucre 40 g
	4) Levure 1 cu. à table
	5) Bien mélanger, il faut briser les mottons de levure.
	6) Huile végétale $\frac{1}{4}$ tasse
	7) 3 gros oeufs à température pièce

Pour la machine à pain, mode sans gluten (#10).