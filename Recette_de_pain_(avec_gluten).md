---
documentclass: extarticle
fontsize: 17pt
header-includes: |
	\usepackage[margin=2cm]{geometry}
---

# Recette de pain (avec gluten)
Il devrait y avoir quelqu'un pour sortir le pain de son moule, dans **quatre heures** environ.

1) **Sec**:
	- Farine de seigle: **120 g**
	- Farine de blé entier à pain: **195 g**
	- Farine blanche: **205 g**
	- Farine de gluten: **35 g**
	- Avoine: **100 g**
	- Sel: **8 à 16 g**

2) **Liquides**:
	1) Eau tiède: **420 g**
	2) Levure: **4 g**
	3) Sucre: **30 à 60 g**
	4) Bien **mélanger**, il faut briser les mottons de levure.
	5) Huile végétale: $\approx$ **20 g**


Mettre tout ensemble dans le récipient de la machine, ne pas oublier la **patente qui tourne**.

Pour la machine à pain (Breadman), **mode** pain blanc (**#1**). 

L'**interrupteur** pour allumer et éteindre la machine se trouve derrière, dans le bas à droite, à côté du fil.

Quand la cuisson est terminée, sortir le pain du moule et le laisser refroidir sur une grille. Quand il a atteint la température pièce, le mettre dans un sac (de tissu ou de plastique) fermé.